<?php


namespace app\models;


class Worker extends \inpassor\daemon\Worker
{
    public function run()
    {
        while (true) {
            $job = Job::getFree();
            if ($job) {
                $job->in_work();
                sleep($job->duration);
                $job->done();
            }
            usleep(10000);
        }
    }
}
