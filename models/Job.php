<?php


namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Job
 * @package app\models
 * @property int $id
 * @property     $create_time Время создания задачи
 * @property int $status
 * @property     $in_work_time Время начала работы
 * @property     $done_time Время завершения работы
 * @property int $duration Длительность работы
 */
class Job extends ActiveRecord
{
    const NEW     = 0;
    const IN_WORK = 1;
    const DONE    = 2;

    /**
     * @return Job|array|ActiveRecord|null
     */
    public static function getFree()
    {
        return static::find()->where(['status' => self::NEW])->orderBy(["id"])->one();
    }

    public function in_work()
    {
        $this->status = self::IN_WORK;
        $this->in_work_time = new \DateTime();
        $this->save();
    }

    public function done()
    {
        $this->status = self::DONE;
        $this->done_time = new \DateTime();
        $this->save();
    }
}
