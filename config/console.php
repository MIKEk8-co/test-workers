<?php

$config_path = __DIR__;
$local_config_path = __DIR__ . '/local';
$require_list = ['params', 'db'];
/** @var array $db */
/** @var array $params */
foreach ($require_list as $require) {
    if (is_file("{$local_config_path}/{$require}.php")) {
        $$require = require "{$local_config_path}/{$require}.php";
    } else {
        $$require = require "{$config_path}/{$require}.php";
    }
}

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,

    'controllerMap' => [
        'daemon' => [
            'class' => 'inpassor\daemon\Controller',
            'uid' => 'daemon', // The daemon UID. Giving daemons different UIDs makes possible to run several daemons.
            'pidDir' => '@runtime/daemon', // PID file directory.
            'logsDir' => '@runtime/logs', // Log files directory.
            'clearLogs' => false, // Clear log files on start.
            'workersMap' => [
                'watcher' => [
                    'class' => 'inpassor\daemon\workers\Watcher',
                    'active' => true, // If set to false, worker is disabled.
                    'maxProcesses' => 3, // The number of maximum processes of the daemon worker running at once.
                    'delay' => 60, // The time, in seconds, the timer should delay in between executions of the daemon worker.
                ],
                'worker' => [

                ]
            ],
        ],
    ],
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
